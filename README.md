To run the demo:

* `vagrant up`
* `vagrant ssh ansible`
* `cd /vagrant/ansible`
* `ansible-playbook -i hosts.yml site.yml`

Vagrant creates 3 Ubuntu 18.04 instances in the same network

* ansible (the controller node)
* mysql (mysql server node)
* tomcat (tomcat9 node)

To check Tomcat9

* `http://localhost:8080`
* `vagrant ssh tomcat` (from the host machine)
* `sudo cat /etc/tomcat9/context.xml`
* check the parameters:
    * `username`
    * `password` (use this for checking mysql)
    * `url`
* check mysql connection
    * `mysql -hmysql.local -umyapp -p myapp` (use password from the previous step)

To check MySQL:

* `vagrant ssh mysql` (from the host machine)
* `mysql -umyapp -p myapp` (use password from the Tomcat9 check)

Hopefully, this demo works as expected!

Software used:

* MacOSX 10.13.6 (host)
* Vagrant 2.2.5
* Ubuntu 18.04 (guest)
* Ansible 2.8.5
* MySQL Server 5.7
* Tomcat 9.0.16