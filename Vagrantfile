# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Distro for all machines
  config.vm.box = "ubuntu/bionic64"

  # Ansible node (for provisioning nodes)
  config.vm.define "ansible" do |machine|
    machine.vm.hostname = "ansible"
    machine.vm.network "private_network", type: "dhcp"
    machine.vm.provision "shell", inline: <<-SHELL
      apt-get update && apt-get install -y software-properties-common &&
        apt-add-repository --yes --update ppa:ansible/ansible &&
        apt-get install -y ansible
      echo "export ANSIBLE_VAULT_PASSWORD_FILE=/vagrant/keys/vault_pass" >>/home/vagrant/.profile
    SHELL
  end

  # MySQL node
  config.vm.define "mysql" do |machine|
    machine.vm.hostname = "mysql"
    machine.vm.network "private_network", type: "dhcp"
  end

  # Tomcat node
  config.vm.define "tomcat" do |machine|
    machine.vm.hostname = "tomcat"
    machine.vm.network "private_network", type: "dhcp"
    machine.vm.network "forwarded_port", guest: 8080, host: 8080
  end

  # Common packages and configuration
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update && apt-get install -y avahi-daemon libnss-mdns aptitude python-apt
    cp /vagrant/keys/id_rsa /home/vagrant/.ssh/id_rsa
    chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
    cat /vagrant/keys/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
  SHELL

end
